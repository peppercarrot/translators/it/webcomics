# Fontforge notes

## Lavi glyph settings

### Bold:

* Element>Styles menu>Change weight.
* Auto, Embolden by 80em units + Fuzz = 0, Counters Squish, Cleanup Self Intersect 'on'.

### Italic:

* Element>Styles menu>Italic.
* Element>Transform feature , Skew by -15°, check "Transform width/kerning/positionning/Round to int".
